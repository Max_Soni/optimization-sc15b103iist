%% bisection mathod for finding minima 
syms x;
f=x^2+1;
fdash=diff(f,x);
y=[-4.5,4];
z1=y(1);
z=y(2);
count=0;
ep=abs(y(1)-y(2));
while ep>0.01
m=min(y);
n=max(y);
o=(m+n)/2;
a=subs(fdash,m);
b=subs(fdash,n);
c=subs(fdash,o);
if(a<0 && c>0)
    y(1)=m;
    y(2)=o;
elseif(c<0 && b>0)
  y(1)=o;
  y(2)=n;
else
     y(1)=o;
     y(2)=o;
end
count=count+1;
ep=abs(y(1)-y(2));
end
minima=[subs(f,y(1)),subs(f,z1),subs(f,z)];
minima=min(minima);
if minima==subs(f,y(1))
    minimaat=y(1);
elseif minima==subs(f,z1)
    minimaat=z1;
else 
    minimaat=z;   
end  

minima_cord=[minimaat,minima,count];
display(minima_cord);