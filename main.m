%% Main.m

syms x;

f=x^2+1;
fdiff=diff(f,x);
interval=[-4.5,4];
tolerance=0.0001;

[minimaat,minima,steps]=Newtonsingle(f,fdiff,x,interval,tolerance);
display([minimaat,minima,steps]);