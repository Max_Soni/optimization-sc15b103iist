%% Newton Graphson Mathod!
function [minimaat,minima,steps]=Newtonsingle(f,fdiff,x,interval,tolerance) 
x1=interval(1);
y1=interval(2);
z=diff(fdiff,x);
xstar=abs(x1-y1)*rand()+x1;
k=subs(z,xstar);
count=0;
while(subs(fdiff,xstar)>tolerance)
   xstar=xstar-subs(fdiff,xstar)/k;
   k=subs(z,xstar);
   count=count+1;
end

minima=[subs(f,y1),subs(f,x1),subs(f,xstar)];
minima=min(minima);
if minima==subs(f,y1)
    minimaat=y1;
elseif minima==subs(f,x1)
    minimaat=x1;
else 
    minimaat=xstar;   
end  
steps=count;
end