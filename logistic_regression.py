# Logistic Regression Problem
# Author Mohit Kumar Soni
import pandas as pd 
from cvxopt import solvers, matrix, spdiag, log, exp, div
solvers.options['show_progress'] = False
data=pd.read_csv("train.csv")
u=pd.DataFrame()
u['Pclass']=data['Pclass']
u['Sex']=data['Sex']
u['Sex']=pd.get_dummies(u.Sex)['female']
u['Fare']=data['Fare']
y=data['Survived']
import sklearn
from sklearn.cross_validation import train_test_split
from sklearn.preprocessing import StandardScaler
scalar=StandardScaler()
u=scalar.fit_transform(u)

m =len(u)
A =matrix(1.0,(m,4))
A[:,0]=u[:,0]
A[:,1]=u[:,1]
A[:,2]=u[:,2]

c = -matrix([sum( uk for uk, yk in zip(u[:,0],y) if yk ),sum(um for um, yk in zip(u[:,1],y) if yk ),sum(um for um, yk in zip(u[:,2],y) if yk ),sum(y)])

# definig the  Maximum likelihood function 
def F(x=None, z=None):
   if x is None: return 0, matrix(1.0, (4,1))
   w = exp(A*x)
   f = c.T*x + sum(log(1+w))
   grad = c + A.T*div(w, 1+w)
   if z is None: return f, grad.T
   H = A.T * spdiag(div(w,(1+w)**2)) * A

   return f, grad.T, z[0]*H


sol = solvers.cp(F)
print(sol['status'])
a=sol['x'][0:3]
b=sol['x'][3]
print(a,b)
try: import pylab
except ImportError: pass
else:
    pylab.figure(facecolor='w')
    nopts = 200
    pts = -2.5+5.5/nopts * matrix(list(range(nopts)))
    w1 = exp(a[0]*pts + b)
    w2 = exp(a[1]*pts + b)
    w3 = exp(a[2]*pts + b)
    pylab.plot(u[:,0], y,'o',u[:,1],y,'x',pts, div(w1, 1+w1),'-',pts, div(w2, 1+w2),'-',pts, div(w3, 1+w3),'-')
    pylab.title('Logistic regression Titanic Survivals')
    pylab.xlabel('u')
    pylab.ylabel('Prob(y=1)')
    pylab.show()
